import matplotlib
matplotlib.use('agg')
import math
from pylab import * 
from sklearn.preprocessing import StandardScaler
from tqdm import tqdm, trange
import tensorflow as tf
import random
import datetime
from sklearn.cluster import KMeans
import h5py
import numpy as np
import pickle 

tf.config.threading.set_intra_op_parallelism_threads(1)
tf.config.threading.set_inter_op_parallelism_threads(1)


class kmeans_cluster:
    def  __init__(self, inputs,n_cluster=10):
        self.inputs = inputs
        self.n_cluster = n_cluster
    #Fitting inputs to kmeans clustering
    #https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
    def fit(self):
        kmeans = KMeans(init='k-means++',n_clusters=self.n_cluster)
        kmeans.fit(self.inputs)
        self.kmeans = kmeans
        self.labels = kmeans.labels_

    #Saving Kmeans function to pickle file so that it can be read in and used in other codes 
    def save(self,filename):
        with open(filename,'wb') as f:
            pickle.dump(kmeans,f)

    #Read in kmeans function from pickle file
    def read(self,filename):
        with open(filename,'wb') as f:
            self.kmeans = pickle.load(f)

    #Use this function to predict labels when using kmeans on new input data
    def predict(self,inputs):
        self.test_labels = self.kmeans.predict(inputs)

#Optional function for smoothing data. For NO2 this is used for 2 reasons:
#1. It is used to smooth input training data (~3 day smoothing) because LSTM
#requires few data gaps, does not like NaNs in data, so do some minor smoothing 
#in training data to avoid this
#2. Use this to smooth the predicted NO2 and OMI NO2. NO2 is NOISY, when looking 
#for signals such as COVID-19, we don't care about day to day variability but are looking 
#at larger overall patterns so the NO2 is smoothed over 30 days. 

#Note this smoothing might not be necessary for all LSTM model applications, it 
#simply depends on the specific data application  
def boxcar(inp,dates,x_len=7):

    #Set up two "paths" for smoothing, one allows for smoothing over 2 dimension 
    #areas so that you can for example smooth over several regions at once 
    out_dates = []
    if inp.ndim == 2:
        n_basins, n_times, = np.array(inp).shape
        smooth = np.zeros((n_basins,n_times-x_len))
        for time in range(x_len,n_times):
            out_dates.append(dates[time-x_len])
            smooth[:,time-x_len] = np.nanmean(inp[:,time-x_len:time],axis=1)
    elif inp.ndim == 1:
        n_times, = np.array(inp).shape
        smooth = np.zeros((n_times-x_len))
        for time in range(x_len,n_times):
            out_dates.append(dates[time-x_len])
            smooth[time-x_len] = np.nanmean(inp[time-x_len:time])


    return smooth, np.array(out_dates)

#The LSDM model! 
class LSTM_Train:
    def  __init__(self, x, y, n_seq, validation_pct,labels,n_cluster=10,seed=42):
        np.random.seed(seed)
        random.seed(seed)

        self.n_basins,    \
        self.n_timesteps, \
        self.n_features   = x.shape
        self.n_clusters = n_cluster + 1
        self.n_seq   = n_seq

        #Picking only so much of your data for training, aka validation_pct
        self.n_valid = n_valid = int(validation_pct * self.n_timesteps)
        self.n_train = n_train = self.n_timesteps - n_valid
        self.max_idx = n_train - n_seq + 1

        self.x_train = x[:, :n_train].reshape((-1, self.n_features))
        self.y_train = y[:, :n_train].reshape((-1, 1))
        labels = np.repeat(labels[:,np.newaxis],self.n_timesteps,axis=-1)[:,:,np.newaxis]

        self.labels_train = labels[:,:n_train].reshape((-1,1))
        self.labels_valid = labels

        #Scaling your training data. Other scalers exist such as MinMaxScaler, but StandardScaler
        #seems to be most commonly used 
        self.x_scaler = x_scaler = StandardScaler()
        self.y_scaler = y_scaler = StandardScaler()
        self.x_train  = x_scaler.fit_transform(self.x_train)
        self.y_train  = y_scaler.fit_transform(self.y_train)


        #Using pickle to store scaler information so it can be read in for future predictions 
        with open('XScaler.pkl','wb') as f:
            pickle.dump(x_scaler,f)

        with open('YScaler.pkl','wb') as f:
            pickle.dump(y_scaler,f)


    #Creating random batches for NN to train on through it's iterations 
    def get_minibatch_generator(self, size):
        indices = np.arange(self.n_basins * self.max_idx)
        np.random.shuffle(indices)

        for batch in np.array_split(indices, len(indices) // size):
            batch = np.unravel_index(    batch, (self.n_basins, self.max_idx)) # Calculate original [basin, ts] inde

            batch = np.ravel_multi_index(batch, (self.n_basins, self.n_train)) # Calculate full combined index
            batch = batch[:, None] + np.arange(self.n_seq)                     # Make each index a slice over the sequence
            x, y  = self.x_train[batch].reshape((-1, self.n_seq, self.n_features)), \
                    self.y_train[batch].reshape((-1, self.n_seq, 1))           # Reshape the data into the final batch

            cluster = self.labels_train[batch].reshape((-1, self.n_seq))
            mask  = np.isfinite(x).all(axis=(1,2)) & np.isfinite(y).all(axis=(1,2))

            if mask.any(): yield x[mask], y[mask][:, -1],cluster[mask]                       # Return last target in each sequence


    #Ok this is where the LSTM model is defined
    def LSTM(self, cell_size):
        tf.keras.backend.clear_session()

        #Embedding layer allows user to pass in clustering information
        inp_emb = tf.keras.Input(shape=(self.n_seq,))
        emb = tf.keras.layers.Embedding(input_dim = self.n_clusters,output_dim=(int(np.ceil(self.n_clusters/2))))(inp_emb)
        inp = tf.keras.Input((self.n_seq,self.n_features))

        #If embedding is used, the embedding input should be concatenated with the other inputs
        conc_inps = tf.keras.layers.Concatenate()([emb,inp])

        #One simply LSTM layer followed by two Dense layers. This is a simply archictecture which can be changed 
        #for different applications by doing things such as adding more LSTM layers, adding Dropout Layer, more Dense layers, etc
        x = tf.keras.layers.LSTM(cell_size)(conc_inps)
        dense = tf.keras.layers.Dense(cell_size,activation='relu')(x)

        #Since NO2 is a linear regression type problem, not binary classification, we use a linear activation for the final layer 
        out = tf.keras.layers.Dense(1,activation='linear')(dense)
        model = tf.keras.Model([inp,inp_emb], out, name="LSTM")

        #Compiling the model with mean squared error loss and Adam optimizer 
        model.compile(loss='mse', optimizer='Adam')
        return model 

    #This is where the model training actually occurs
    def fit(self, cell_size, batch_size, n_epochs):
        self.model = self.LSTM(cell_size)
        print(self.model)
        losses     = []

        #Looping through the training one epoch at a time 
        for epoch in trange(n_epochs):
            for x_batch, y_batch,cluster in self.get_minibatch_generator(batch_size):
                #Making sure that the current batch for training has at least 3 valid data points so that it really does use memory 
                if len(y_batch[:,0]) < 3:
                    continue
                history = self.model.fit([x_batch,cluster], y_batch, batch_size=batch_size, shuffle=False, epochs=1, verbose=0)
                losses.append(history.history['loss'][-1])
                
        #Prints model structure
        print(self.model.summary())

        #Saving LSTM model so that it can be used in future predictions 
        self.model.save('Test_model')
        plt.plot(losses)
        plt.savefig('Losses.png')
        plt.clf()
        plt.close()
        return losses



#Reading all data in from hdf5 file and storing in a python dictionary
def read_h5(filename):
    data = {}
    f = h5py.File(filename,'r')
    for key in f.keys():
        data[key] = f[key][:]
    return data

if __name__ == "__main__":

    #Reading sample NO2 file
    no2_train_data = read_h5('NN_NO2_Inputs_2018_Test.h5')
    cities = no2_train_data['City']
    no2_train_data_mean = {}


    no2_train_data['POPDENSITY'][77,:] = 3500.
    #Averaging NO2 data over the year so that cities can be clustered based on mean inputs
    for key in no2_train_data.keys():
        if no2_train_data[key].ndim==2:
            no2_train_data_mean[key] = np.nanmean(no2_train_data[key],axis=-1)

    

    
    #Stacking desired cluster inputs. This is just a sample, can use any desired inputs for clustering
    train_cluster_inp = np.swapaxes(np.vstack((no2_train_data_mean['T2M'],no2_train_data_mean['GMI_NO2'],no2_train_data_mean['POPDENSITY'])),0,1)
    
    #Performing Kmeans clustering on inputs, set up now for kmeans to use 10 clusters
    n_cluster = 20

    no2_cluster = kmeans_cluster(train_cluster_inp,n_cluster=n_cluster)
    
    no2_cluster.fit()
    #stop

    dates = []

    for i in range(365):
        dates.append(datetime.datetime(2018,1,1)+datetime.timedelta(days=i))

    no2_train_data_smooth = {}
    
    for key in no2_train_data.keys():
        if (key == 'City') | (no2_train_data[key].ndim ==1):
            continue
        no2_train_data_smooth[key], nn_train_dates = boxcar(no2_train_data[key],dates,x_len=3)


    train_inputs = np.dstack((no2_train_data_smooth['T2M'],np.cos(np.radians( no2_train_data_smooth['SZA'])),no2_train_data_smooth['U10M'],no2_train_data_smooth['V10M'],no2_train_data_smooth['GLER']))

    #Training sample model for year 2018 with 2 inputs of surface temperature and solar zenith angle. This is a simplified 
    #model but still works surprisingly well
    #Here n_seq is the length of memory in this case it is in days, n_features is simply the number of input features (T2M, SZA)
    #Cell size is the size of the LSTM layer
    n_seq = 7
    n_features = train_inputs.shape[-1]
    cell_size = 32
    batch_size = 32
    epochs = 80
    validation_pct = 0.2

    lstm_model = LSTM_Train(train_inputs,no2_train_data_smooth['NO2'],n_seq,validation_pct,no2_cluster.labels,n_cluster=c_cluster)
    lstm_model.fit(cell_size,batch_size,epochs)


    #Reading in NO2 data from 2019 for trsting the newly trained model 
    no2_test_data = read_h5('NN_NO2_Inputs_2019_Test.h5')
    no2_test_data_mean = {}


    no2_test_data['POPDENSITY'][77,:] = 3500.

    #Averaging NO2 data over the year so that cities can be clustered based on mean inputs
    for key in no2_test_data.keys():
        if no2_test_data[key].ndim==2:
            no2_test_data_mean[key] = np.nanmean(no2_test_data[key],axis=-1)


    #Stacking desired cluster inputs. This is just a sample, can use any desired inputs for clustering
    test_cluster_inp = np.swapaxes(np.vstack((no2_test_data_mean['T2M'],no2_test_data_mean['GMI_NO2'],no2_test_data_mean['POPDENSITY'])),0,1)
    inds = np.where(np.isnan(test_cluster_inp))

    #Predict labels for new dataset 
    no2_cluster.predict(test_cluster_inp)
    test_labels = np.array(no2_cluster.test_labels,dtype=np.float32)

    test_dates = []

    for i in range(365):
        test_dates.append(datetime.datetime(2019,1,1)+datetime.timedelta(days=i))

    no2_test_data_smooth = {}

    #Again smoothing test input data to remove gaps in data
    for key in no2_test_data.keys():
        if (key == 'City') | (no2_test_data[key].ndim ==1):
            continue
        no2_test_data_smooth[key], nn_test_dates = boxcar(no2_test_data[key],test_dates,x_len=3)


    test_inputs = np.dstack((no2_test_data_smooth['T2M'], np.cos(np.radians( no2_test_data_smooth['SZA'])),no2_test_data_smooth['U10M'],no2_test_data_smooth['V10M'],no2_test_data_smooth['GLER']))

    #Loading the newly trained model 
    model = tf.keras.models.load_model('Test_model')

    #Loading the scaling fucntions 
    with open('XScaler.pkl','rb') as f:
        x_scaler = pickle.load(f)
    with open('YScaler.pkl','rb') as f:
        y_scaler = pickle.load(f)

    x   = x_scaler.transform(test_inputs.reshape((-1, n_features))).reshape(test_inputs.shape)
    test_labels = np.repeat(test_labels[:,np.newaxis],x.shape[1],axis=-1)

    for test_city in ['Baltimore','New York','Los Angeles','Rome','Paris','Amsterdam']:
        est = []
        

    
        #test_city = 'Baltimore'
        city_ind, =  np.where(cities == test_city)

        #Making a sample prediction for the provided city above
        for basin in x[city_ind[0]:city_ind[0]+1]:
            
            est_tmp = np.array([model.predict([np.array(basin[i-n_seq:i]).reshape(-1,n_seq,n_features,),test_labels[city_ind,i-n_seq:i].reshape(-1,n_seq)]) for i in range(n_seq,len(basin)+1)])
            est = np.hstack((est,est_tmp.flatten()))
        est = np.array(est).reshape(1,-1,1)

        estimated_no2 = y_scaler.inverse_transform(est.reshape((-1, 1))).reshape(est.shape)
        
        
        #Rest of code makes simple plot of the NO2 prediction
        fig,axes = plt.subplots(2,figsize=(10,7))
        axes[0].plot(nn_test_dates[n_seq-1:],estimated_no2.flatten(),color='b',label='NN Estimated NO2')
        axes[0].plot(nn_test_dates[n_seq-1:],no2_test_data_smooth['NO2'][city_ind,n_seq-1:].flatten(),color='r',label='OMI NO2')
        
        axes[0].legend(ncol=2,loc='upper center')
        
        smooth_nn_no2, smooth_dates  =  boxcar(estimated_no2.flatten(),nn_test_dates[n_seq-1:],x_len=30)
        smooth_omi_no2, smooth_dates  =  boxcar(no2_test_data_smooth['NO2'][city_ind,n_seq-1:].flatten(),nn_test_dates[n_seq-1:],x_len=30)
        axes[1].plot(smooth_dates,smooth_nn_no2,color='b')
        axes[1].plot(smooth_dates,smooth_omi_no2,color='r')
        
        axes[0].set_ylabel('Raw NO2',fontsize=15)
        axes[1].set_ylabel('30 Day Smooth NO2',fontsize=15)
        axes[1].set_xlabel('Date',fontsize=20)
        fig.autofmt_xdate()
        
        for ax in axes.flatten():
            ax.set_ylim(0,10)
            ax.set_xlim(datetime.datetime(2019,1,1),datetime.datetime(2019,12,31))
        plt.savefig(test_city+'_NN_NO2.png')
        plt.clf()
        plt.close()
